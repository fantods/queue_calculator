# Ruby/Python M/M/c/k Queue System Simulation

Requires Ruby v. 2.3.3 and Python v3.6.0 (might work with previous versions, including python2)

Program has been tested on 64-bit Windows 10 and 64-bit OSX El Capitan

## Usage:

- `cd` into `queue_calculator` directory
- `bundle install`
- `rake`

To create your own m/m/c/k simulation:

- edit the example in `lib/queue_calculator.rb` (follow the guide)
- `rake` from project root

## Note:

There was some difficulty in the ruby-based m/m/c/k calculator, returning incorrect output due to overflowing.

Currently, the m/m/c/k queue is implemented in both ruby and python and this repository also contains a ruby implementation of an m/m/c queue as well.

The python calculator should work on most Cython distributions, it was written in Python 3 but has been tested with Python 2 and does run correctly.

This might not make the gem work as expected however, since it requires both a ruby and a python interpreter installed on your system (to get the proper M/M/C/K output atleast).

Easiest way  to test the output is to write your simulator parameters into the example in `lib/queue_calculator.rb` and execute them with `rake`