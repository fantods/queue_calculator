from __future__ import division
from math import pow
from math import factorial as fac
import time
import sys

class MMCK:
    
    def __init__(self, lambda_v, mu_v, servers, buffer):
        """
        Initialize parameters
        """
        # Init parameters to start simulation
        self.lambda_v = lambda_v
        self.mu_v = mu_v
        self.servers = servers
        self.buffer = buffer 
        self.p0_optimizer = False
        self.p0_aux = 0
        # Calculate Rho parameter
        self.rho = self.lambda_v / (self.servers * self.mu_v)
        # Calculate r parameter
        self.r = self.lambda_v / self.mu_v
    
    def stability_condition(self):
        """
        Checking system stability condition
        """
        return self.rho < 1
        
    def p0(self):
        """
        Probability of "0" transactions
        """
        if not self.p0_optimizer:
            self.p0_optimizer = True
            acum = 0
            for n in range(0, self.servers):
                acum += pow(self.r, n) / fac(n)
            aux = ((pow(self.r, self.servers))/(fac(self.servers))) * (((1 - pow(self.rho, self.buffer - self.servers + 1)) / (1 - self.rho)))
            self.p0_aux = 1 / (acum + aux)
            return self.p0_aux
        else:
            return  self.p0_aux
        
        
    def pn(self,n):
        """
        Probability of "n" transactions, if n > 0 and n <= k
        """
        assert(n > 0 and n <= self.buffer)
        
        if n < self.servers:
            return (pow(self.lambda_v, n) / (fac(n) * pow(self.mu_v, n))) * self.p0()
        elif self.servers <= n:
            return pow(self.lambda_v, n) / (pow(self.servers, (n - self.servers)) * fac(self.servers) * pow(self.mu_v, n)) * self.p0()
        
        
    def Lq(self):
        """
        Mean queue length
        """
        a = (self.p0() * pow(self.r, self.servers) * self.rho) / (fac(self.servers) * pow(1-self.rho, 2))
        b = 1 - pow(self.rho, self.buffer - self.servers + 1) - ((1-self.rho) * (self.buffer - self.servers + 1) * pow(self.rho, self.buffer - self.servers))
        return a * b
        
    def L(self):
        """
        Mean number of customers in system
        """
        return self.Lq() + (self.r * 1 - self.pn(self.buffer))
        
        
    def W(self):
        """
        Mean wait time in systev
        """
        return self.L() / (self.lambda_v * (1 - self.pn(self.buffer)))
        
        
    def Wq(self):
        """
        Mean wait time in queue
        """
        return (self.L() / (self.lambda_v * (1 - self.pn(self.buffer)))) - (1 / self.mu_v)



def handle_sys_call(args):
    arrival = float(args[1])
    departure = float(args[2])
    servers = int(args[3])
    buffer = int(args[4])
    simulation = MMCK(arrival, departure, servers, buffer)
    print_output(simulation)

def print_output(sim):
    print("\n- System Parameters")
    print("\tArrival:\t\t%0.1f" % sim.lambda_v)
    print("\tDeparture:\t\t%0.1f" % sim.mu_v)
    print("\tServers:\t\t%d" % int(sim.servers))
    print("\tQueue Capacity: \t%d\n" % int(sim.buffer))

    print("\tSystem Stable?\t\t%r" % (int(sim.stability_condition()) == 1))
    print("\tSystem Utilization:\t%0.2f%%" %  (sim.rho * 100))

    print("\n- Queue Information")
    print("\tMean transactions in system (L)\t= %0.4f" % sim.L())
    print("\tMean queue length (Lq)\t\t= %0.4f" % sim.Lq())
    print("\tMean time spent in system (W)\t= %0.4f" % sim.W())
    print("\tMean time waiting in queue (Wq)\t= %0.4f" % sim.Wq())

    print("\n- Probability Distribution")
    acum = 0
    acum += sim.p0()
    print("\tP_0 = %0.10f" % sim.p0())
    for n in range(1, sim.buffer + 1):
        a = sim.pn(n)
        if n < 10 or n == sim.buffer:
            print("\tP_%s = %0.15f" % (n, a))
        elif n == 10 and sim.buffer != 11:
            print("\tP_%s = ..... " % n)
            print("\t..... ")
        acum += a
    print("\t[Total Probability: %s]\n" % round(acum))


handle_sys_call(sys.argv)