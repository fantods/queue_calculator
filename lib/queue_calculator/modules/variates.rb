# module to generate random variates
# Random Variate Generation Module
module Variates

    # Generates uniform random variate at: interval +/- variance
    # Example: generate_uniform(8,3) would generate within range: { 5..11 }
    #
    # @param mean std_dev seed [Number, Number, nil]
    # @return [Float] returns generated random variate
    def self.generate_uniform(mean, std_dev, seed = nil)
        seed = Random.new_seed if seed.nil?
        r = Random.new(seed)
        lower = mean - std_dev
        upper = mean + std_dev
        return r.rand * (upper - lower) + lower
    end

    # Generates exponential random variate
    #
    # @param mean std_dev seed [Number, Number, nil]
    # @return [Float] returns generated random variate
    # FIXME: using a gem for now, maybe change to my own implementation?
    def self.generate_exponential(mean, seed = nil)
        seed = Random.new_seed if seed.nil?
        r = Random.new(seed)
        return (-mean * Math.log(1.0 - r.rand))
    end
end