module QueueCalculator
    # Input Validation for SimulatorParams
    module Validator

        # Validates that num is a positive Integer or Float
        #
        # @param num [Integer, Float]
        # @raise [Exception] will return error if number is not the proper type
        # @return [Integer, Float] returns the number if its valid
        def self.validate_number(num)
            if ((num.kind_of?(Integer) || num.kind_of?(Float)) && num > 0)
                return num
            else
                raise "ERROR: Number must be an Integer or Float"
            end
        end

        # Validates that num is a positive Integer
        #
        # @param num [Integer, Float]
        # @raise [Exception] will return error if number is not the proper type
        # @return [Integer] returns the number if its valid
        def self.validate_int(num)
            if (num.kind_of?(Integer) && num > 0)
                return num
            else
                raise "ERROR: Number must be an Integer or Float"
            end
        end
    end
end