require "queue_calculator/classes/builder"
require 'pry'

# DSL helper module
module QueueCalculator
    module DSL

        # Builds SimulationParams objects using recursive instance evaluation
        #
        # @param block [Block] pass a block to this method with the appropriate values
        # @return [Object] returns a simulation
        def self.create_simulation(&block)
            params = self.eval(SimulationBuilder.new, &block).build
            
            arr = params.arrival.to_s
            dep = params.departure.to_s
            serv = params.servers.to_s
            buf = params.buffer.to_s

            cwd = Pathname.new(Dir.pwd)
            # python-based m/m/c/k queue
            exec("python #{cwd.to_s}/lib/mmck_sim.py #{arr} #{dep} #{serv} #{buf}")

            # ruby-based m/m/c/k queue (suffer from float overflow errors)
            # Simulator.new(params)

            # ruby-based m/m/c queue
            # Queue.new(params)
        end

        # Generic DSL evaluation helper
        #
        # @param object block [Object, Block] instance evaluates the block in the context of the object
        # @return [Object] returns the built SimulationParams object
        def self.eval(object, &block)
            object.instance_eval(&block)
        end

    end
end