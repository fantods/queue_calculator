require 'queue_calculator/modules/dsl'
require 'queue_calculator/classes/queue'
require 'queue_calculator/classes/simulator'
require 'queue_calculator/classes/builder'

module QueueCalculator

    # Main entry point into library
    def self.create_queue(&block)
        Main.new(&block)
    end

    class Main
        include DSL
        # Creates and runs the simulation with given parameters (if valid)
        #
        # @param block [block] block containing simulation parameters
        def initialize(&block)
            if block_given?
                DSL.create_simulation(&block)
            else
                raise "ERROR: Must submit a block to create_queue"
            end
        end
    end
end