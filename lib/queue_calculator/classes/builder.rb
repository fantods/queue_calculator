require "queue_calculator/modules/validator"
require "queue_calculator/classes/parameters"

# MMC Module
module QueueCalculator
    # Builder pattern passed to DSL
    # initializes simulations with passed parameters
        # Capacity: number of servers (only supports 1 at the moment)
        # Arrrival time
        # Departure time
    class SimulationBuilder

        # Sets server capacity (defaults to 1)
        #
        # @param value [Number] the accepted queue type (defaults to :queue)
        # @return [Self] adds the queue type to the builder object
        def servers(value=nil)
            @servers = value
            self
        end

        # Sets buffer size (defaults to infinity)
        #
        # @param value [Number] the accepted queue type (defaults to :queue)
        # @return [Self] adds the queue type to the builder object
        def buffer(value=nil)
            @buffer = value
            self
        end

        # Sets the arrival value (accepts integer or float)
        #
        # @param arrival [Integer, Float] accepts a Number, which is validated
        # @return [Self] adds the interarrival rate to the builder object
        def arrival(arrival=nil)
            @arrival = arrival
            self
        end

        # Sets the departure value (accepts integer or float)
        #
        # @param departure [Integer, Float] accepts a Number, which is validated
        # @return [Self] adds the interarrival rate to the builder object
        def departure(departure=nil)
            @departure = departure
            self
        end

        # Constructs the Simulation Object
        #
        # @return [Self] the fully built object
        def build
            SimulationParams.new(
                @servers,
                @buffer,
                @arrival, 
                @departure
            )
        end
    end
end