module QueueCalculator
    # Sets up Simulation Parameters to be passed to Simulator
    class SimulationParams
        include Validator

        # public readable instance variables
        attr_reader :servers, :buffer, :arrival, :departure

        # Sets the instance variables of a Simulation
        # these are validated and then passed to Simulator
        #
        # @param servers buffer arrival departure [Number, Number, Number, Number] sets the parameters
        # @return [Self] returns the parameters object
        def initialize(servers = nil, buffer = nil, arrival, departure)
            @servers = servers.nil? ? 1 : Validator.validate_int(servers)
            @buffer = buffer.nil? ? big_int : Validator.validate_int(buffer)
            @arrival = Validator.validate_number(arrival)
            @departure = Validator.validate_number(departure)
        end

        def buffer_limit
            big_int
        end

        private

        def big_int
            bits = [42].pack('i').size * 16
            2 ** (bits - 2) - 1
        end

    end
end