module QueueCalculator
    # Simulates an M/M/c queue with 'c' servers and an infinite buffer
    #
    # Interarrival time of a transaction is a Possion random variate
    # Service time of a transaction is an Exponential random variate
    #
    # If a transaction arrives and there is atleast 1 idle server,
    # this transaction would be served, other wise it enters the queue
    # 
    # Accepted Parameters of M/M/c Queue:
    #   Arrival Rate
    #   Departure Rate
    #   Number of Servers (Capacity)
    class Queue

        # Creates Simulator object and initializes all instance variables
        #   it assumes that the instance variables are all of the correct type
        #
        # @param builder [Object] accepts SimulationParams object
        # @return [Self] returns self
        def initialize(builder)
            # lambda
            @arrival = Float(builder.arrival)
            # mu
            @departure = Float(builder.departure)
            # c
            @capacity = builder.capacity
            # utilization
            @rou = @arrival / @departure / @capacity

            # factor parameters
            @power_term = 1.0
            @factor_term = 1.0
            @pre_sum = 1.0
            # loop 1 to @capacity for each term and pre_sum
            1.upto(@capacity) do |i|
                @power_term *= @arrival / @departure
                @factor_term /= i
                @pre_sum += @power_term * @factor_term
            end

            # final term : 1 / (capacity!) * (arrival / departure)^capacity
            @final_term = @power_term * @factor_term
            # p0 : Probability of no transactions in the queue
            @p0 = 1.0 / (@pre_sum + @final_term / (1 - @rou))
            # pC : Probability of exactly `capacity` transactions in queue (all servers busy)
            @pc = @final_term * @p0
            # probability sum : p0 + p1 + p2 + ... pC - pC
            @prob_sum =  @pre_sum * @p0
        end

        # Prints the output
        #
        # @return [Self] 
        def run
            print_output
        end

        # Returns the factorial of n
        #
        # @param n [Integer] value you want to factorial
        # @return [Number] returns factorial of n
        def factorial(n)
            (1..n).reduce(:*) || 1
        end

        # Returns the probability of 'n' transactions in the system
        #
        # @param n [Integer] number of transactions
        # @return [Number] returns the probability of n transactions
        def get_pk(n)
            if n.zero?
                return @p0
            elsif n == @capacity
                return @pc
            elsif n < @capacity
                # @factor_term
                factor_term = 1.0 / factorial(n)
                power_term = Float((@arrival / @departure) ** n)
            else
                return @final_term * Float(@rou ** (n - @capacity)) * @p0
            end
        end

        # Returns the probability of a transaction needing to enter the queue
        #   P(W>0) = 1 - P(N < C)
        #
        # @return [Number] returns the probability sum ratio
        def queue_probability
            return 1.0 - @prob_sum
        end

        # Returns the probability when the server is idle
        #   P(N = 0)
        #
        # @return [Number] returns the server idle probability 
        def idle_probability
            return @p0
        end

        # Returns the mean number of transactions in the system
        #   transactions both in the queue and in service
        #
        # @return [Number] returns the mean number of transactions
        def mean_transactions
            return @rou / (1 - @rou) * queue_probability + @capacity * @rou
        end

        # Returns the mean time a transaction spends in the queue
        #
        # @return [Number] returns the mean transaction wait time
        def mean_queue_time
            return queue_probability / (@capacity * @departure - @arrival)
        end

        # Returns the mean number of transactions in the queue
        #   given that there are any transactions in the queue
        #
        # @return [Number] returns the mean transactions in the queue
        def mean_queued_transactions
            return @final_term * @p0 / (1.0 - @rou) / (1.0 - @rou)
        end

        # Returns the mean time a transaction must wait if it has to wait at all
        #
        # @return [Number] returns the mean transactions wait time
        def mean_transaction_wait_time
            if queue_probability.zero?
                return 0
            end
            return mean_queued_transactions / (queue_probability * @arrival)
        end

        # Returns the mean time of transactions spent in the system
        #   in both the queue and in service
        #
        # @return [Number] returns the average response time
        def mean_response_time
            return mean_queue_time + (1.0 / @departure)
        end

        # Returns the mean number of transactions in the system
        #
        # @return [Number] returns the average transactions in the system at any time
        def mean_transactions_in_system
            return mean_response_time * @arrival
        end

        # Returns the mean number of busy servers in the system
        #
        # @return [Number] returns the average number of busy servers
        def mean_busy_servers
            return @arrival / @departure
        end

        # Returns the probability of a transaction when it is larger than the queueing time
        #   This is P(W > queue time) = 1 - P(W <= queue time)
        #
        # @param time [Number] the queue time to compare
        # @return [Number] returns the probability of a transaction > queue time
        def probability_when_queue_time(time)
            first_term = @pc / (1.0 / @rou)
            exponent = @capacity * @departure * (1.0 - @rou) * time
            second_term = Math.exp(exponent)
            return first_term * second_term
        end

        # Prints the output to console
        #
        # @return [String] returns a multi-line string out the systems statistics
        def print_output
            queue_times = [0.00001, 0.0001, 0.001, 0.01]
            puts
            puts "Running Simulation with Parameters:"
            puts "Number of Servers:\t#{@capacity}"
            puts "Interarrival Rate:\t#{Integer(@arrival)}"
            puts "Departure Rate:\t\t#{Integer(@departure)}"
            puts 
            puts "Results:"
            puts "Mean Number of Transactions in the System:\t#{mean_transactions_in_system.round 6}"
            puts "Mean Number of Busy Servers:\t\t\t#{mean_busy_servers.round 6}"
            puts "Idle Server Probability:\t\t\t#{idle_probability.round 6}"
            puts "Probability of a Transaction Forced to Wait:\t#{queue_probability.round 6}"
            puts "Mean Wait Time of Transactions Forced to Wait:\t#{mean_transaction_wait_time.round 6}"
            puts "Mean Queuing Time:\t\t\t\t#{mean_queue_time.round 6}"
            queue_times.each do |time|
                if time == queue_times.last
                    puts "P[Queuing Time >= #{time}]\t\t\t\t#{(probability_when_queue_time(time) * 100).round 6}%"
                else
                    puts "P[Queuing Time >= #{time}]\t\t\t#{(probability_when_queue_time(time) * 100).round 6}%"
                end
            end
            puts
        end

    end
end
