module QueueCalculator

    # m/m/c/k queue
    class Simulator

        def initialize(builder)
            # parameter builder
            @builder = builder
            # arrival rate
            @lambda = (@builder.arrival)
            # service rate
            @mu = (@builder.departure)
            # number of servers
            @servers = (@builder.servers)
            # size of buffer/line
            @buffer = (@builder.buffer)

            # probability parameters
            @p0_opt = false
            @p0_aux = 0

            # server utilization parameter
            @rho = @lambda.to_f / (@servers * @mu)

            # calculate r parameter
            @r = @lambda.to_f / @mu
        end

        def run
            print_output
        end

        private 

        # check if system is stable
        def stable_system?
            @rho < 1
        end

        # probability of '0' state
        def p0
            unless @p0_opt
                @p0_opt = true
                acum = 0.0
                0.upto(@servers) do |i|
                    acum += (@r ** i) / factorial(i)
                end
                a = (@r ** @servers) / factorial(@servers)
                b = (1 - (@rho ** (@buffer - @servers + 1))) / (1 - @rho)
                aux = a * b
                @p0_aux = 1.0 / (acum + aux)
                return @p0_aux
            else
                return @p0_aux
            end
        end

        # probability of 'n' state, if n > 0 and n <= buffer 
        def pn(n)
            if n > 0 && n <= @buffer
                if n < @servers
                    value = (@lambda ** n) / ((factorial(n) * (@mu ** n)) * p0)
                elsif @servers <= n
                    value =  (@lambda ** n)/ (@servers ** (n - @servers)) * factorial(@servers) * (@mu ** n) * p0
                end
                return value
            end
        end

        # Lq value        
        def paramLq
            a = (p0 * (@r ** @servers) * @rho)/ (factorial(@servers) * ((1 - @rho) ** 2))
            b = 1 - (@rho ** (@buffer - @servers + 1))  - (1 - @rho) * (@buffer - @servers + 1) * (@rho ** (@buffer - @servers))
            return a * b
        end

        # L value        
        def paramL
            return @r * (1 - pn(@buffer - 1))
        end

        # W value
        def paramW
            return paramL / (@lambda * (1 - pn(@buffer)))
        end

        # Wq value
        def paramWq
            return paramL / (@lambda * (1 - pn(@buffer))) - (1 / @mu)
        end

        # helper factorial function
        def factorial(n)
            (1..n).inject(:*) || 1
        end

        # prints output
        def print_output
            puts
            puts "Running Simulation with Parameters:"
            puts "Number of Servers:\t#{@servers.round}"
            puts "Queue Capacity:\t\t#{@buffer == @builder.buffer_limit ? 'Infinite' : @buffer.round}"
            puts "Arrival Rate:\t\t#{@lambda.round}"
            puts "Departure Rate:\t\t#{@mu.round}"
            puts "System Stable:\t\t#{stable_system?}"
            puts "System Utilization:\t#{(@rho * 100).round 2}%"
            puts 
            puts "Average Customers in System (L):\t#{paramL}"
            puts "Average Customers in Queue (Lq):\t#{paramLq}"
            puts "Average Time in System (W):\t\t#{paramW}"
            puts "Average Time Waiting in Queue (Wq):\t#{paramWq}"


            puts "\nP[#{0} customers in system]  =\t\t%f" % p0
            1.upto(@buffer + 1) do |i|
                prob = pn(i)
                if i < 10 || i == @buffer
                    puts "P[#{i} customers in system]  =\t\t%f" % prob
                elsif i == 10 && @buffer != 11
                    puts "..."
                end
            end
        end
    end
end