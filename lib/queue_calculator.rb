require "queue_calculator/version"
require "queue_calculator/main"

# Default Queue (usage example)
module Runner
    # Format (pass a block to MMC.create_queue with accepted parameters):
        # MMC.create_queue do
        #     servers
        #     buffer
        #     arrival
        #     departure
        # end
        
    # Accepted Parameters:
    # servers = number of servers in the queue
    #       must be a positive integer (defaults to 1 if not present)
    # buffer = length of queue
    #       must be a positive integer (defaults to a very large integer if not present)
    # arrival = mean arrival rate (in user-defined time scale)
    #       must be a positive integer or float
    # departure = mean departure rate (in user-defined time scale)
    #       must be a positive integer or float
    def self.default

        # Enter Parameters Here!
        QueueCalculator.create_queue do 
            servers 2
            buffer 100
            arrival 20
            departure 15
        end


    end
end